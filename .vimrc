" Change the leader from \ to ,
let mapleader = ","

set nocompatible

if $KBD_LAYOUT == 'colemak' || has("gui_running")

  noremap n j|noremap <C-w>n <C-w>j
  noremap e k|noremap <C-w>e <C-w>k
  noremap N J|noremap <C-w>N <C-w>J
  noremap E K|noremap <C-w>E <C-w>K
  noremap s h|noremap <C-w>s <C-w>h
  noremap S H|noremap <C-w>S <C-w>H
  noremap t l|noremap <C-w>t <C-w>l
  noremap T L|noremap <C-w>T <C-w>L

  noremap h s|noremap <C-w>h <C-w>s
  noremap f e|noremap <C-w>f <C-w>e
  noremap F E|noremap <C-w>F <C-w>E
  noremap k n|noremap <C-w>k <C-w>n
  noremap K N|noremap <C-w>K <C-w>N

  noremap j f|noremap <C-w>j <C-w>f
  noremap J F|noremap <C-w>J <C-w>F
  noremap l t|noremap <C-w>l <C-w>t
  noremap L T|noremap <C-w>L <C-w>T

  "L & H is remaped to S & T so unmap L & H
  nnoremap L <Nop>
  nnoremap H <Nop>
endif

"Remap ctrl-n to be save. :w<cr> is not helping my rsi!
nnoremap <C-n> :w<cr>

" swap characters. Works like ctl-t in zsh.
nmap <C-\> Xp

" Position the cursor at the beginning of the redo
nmap . .`[

set spelllang=en_gb

if &compatible
  set nocompatible
endif

" To make the plugin system work run the following:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

call plug#begin('~/.vim/plugged')

Plug 'Shougo/vimproc.vim', {'build': 'make'}

Plug 'skywind3000/asyncrun.vim'

Plug 'lifepillar/vim-mucomplete'

Plug 'Shougo/unite.vim'

Plug 'elixir-lang/vim-elixir'
Plug 'chr4/nginx.vim'

Plug 'cespare/vim-toml'
Plug 'hashivim/vim-terraform'
Plug 'hashivim/vim-hashicorp-tools'
Plug 'tpope/vim-markdown'

Plug 'jceb/vim-orgmode'
Plug 'vmchale/dhall-vim'
Plug 'jvirtanen/vim-hcl'

Plug 'Twinside/vim-hoogle'
Plug 'michaeljsmith/vim-indent-object'

Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'mattn/vim-lsp-settings'
Plug 'frigoeu/psc-ide-vim'

Plug 'purescript-contrib/purescript-vim'

Plug 'tomtom/tlib_vim'
Plug 'ervandew/supertab'
Plug 'MarcWeber/vim-addon-mw-utils'

Plug 'altercation/vim-colors-solarized'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'

Plug 'Kris2k/matchit'
Plug 'noprompt/vim-yardoc'

Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'

Plug 'jamessan/vim-gnupg'

Plug 'mtth/scratch.vim'
Plug 'mbbill/undotree'

Plug 'airblade/vim-gitgutter'

Plug 'ConradIrwin/vim-bracketed-paste'

Plug 'vim-scripts/dbext.vim'

" Initialize plugin system
call plug#end()


" Required:
filetype plugin indent on
syntax enable

ab lbz laɪbnɪts

" This currently conflicts with ctrl-n (save)
" set runtimepath+=/home/rgh/.vim/yankring

nnoremap <silent><leader>s :%s/\s\+$//<cr>:let @/=''<cr>`'
nnoremap <silent><leader>w :set nowrap!<cr>
nnoremap <silent><leader>m :UndotreeToggle<cr>
nnoremap <silent><leader>r :set filetype=haskell<cr>
nnoremap <silent><leader>c :set spell!<cr>

nnoremap <silent><leader>l :ls<CR>
" nnoremap <silent><leader>a :ALEToggle<CR>

nnoremap <silent><leader>. :if exists("g:syntax_on") <Bar> syntax off <Bar> else <Bar> syntax enable <Bar> endif <CR>

nnoremap <silent><leader>f :let @*=expand("%")<CR>
nnoremap <silent><leader>F :let @*=expand("%:p")<CR>

let g:acp_enableAtStartup = 0

" Perform zsh-like prompt expansion using the template {prompt}.  See
" "EXPANSION OF PROMPT SEQUENCES" in zshmisc(1).
" function s:ZshPromptExpn(prompt)
"     return system("print -Pn " . shellescape(a:prompt))
" endfunction

" function s:MyTitle()
"   return s:ZshPromptExpn("%m:%-3~ ") . v:progname . " " . fnamemodify(expand("%:f"), ":.")
" endfunction

" nmap <buffer> n   <Plug>(unite_loop_cursor_down)
" nmap <buffer> k   <Plug>(unite_loop_cursor_up)

" nmap n <Plug>(unite_loop_cursor_down)
" nmap k <Plug>(unite_loop_cursor_up)

" inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

inoremap <C-;> xP


syntax enable

filetype on
filetype plugin on
filetype indent on

autocmd BufNewFile,BufReadPost *.md set filetype=markdown

highlight Pmenu ctermbg=238 gui=bold

autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0

autocmd FileType cpp,make,go set listchars=tab:\ \ ,trail:→,extends:»,nbsp:␣

autocmd FileType haskell set formatprg=stylish-haskell


let g:solarized_bold=0
let g:solarized_underline=0
let g:solarized_termtrans=1
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"
let g:solarized_termtrans=1

syntax enable
set background=dark
colorscheme solarized


:nohlsearch

let g:allml_global_maps = 1
let loaded_matchparen = 1

" rails stuff.

if $USER != 'root'
  set modeline
else
  set nomodeline
endif

set pastetoggle=<c-v>

" default options
set formatoptions=tcq1j

set list
set listchars=trail:→,extends:»,nbsp:␣


" default comment string
set commentstring=#\ %s

" Language specific comment strings

autocmd FileType c set commentstring=//\ %s
autocmd FileType cpp set commentstring=//\ %s

autocmd FileType sql set commentstring=--\ %s
autocmd FileType cabal set commentstring=--\ %s
autocmd FileType cabalproject set commentstring=--\ %s
autocmd FileType purescript set commentstring=--\ %s


set viminfo^=!

" Time to wait after ESC (default causes an annoying delay)
set timeout timeoutlen=500 ttimeoutlen=10

set autoindent


" Visual
set novisualbell  " No blinking .
set noerrorbells  " No noise.


set expandtab
set softtabstop=2
set tabstop=2
set shiftwidth=2
autocmd FileType !email set textwidth=89

set nonumber

" See http://items.sjbach.com/319/configuring-vim-right for an
" explanation of the following:
set hidden
set wildmode=list:longest,full

set directory=~/.vim/tmp

" persistent undo.
if v:version >= 703
  set undofile
  set undodir=~/.vim/tmp
endif

" Shorten messages
set shortmess=aI

" Don't add two spaces when joining lines
set nojoinspaces

autocmd FileType purescript ab forall ∀
autocmd FileType purescript ab -> →
autocmd FileType purescript ab <- ←


if exists('$TMUX')
  autocmd BufEnter * call system("tmux rename-window " . expand("%:t"))
  autocmd VimLeave * call system("pwd")
else
  autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
endif

set title

"##########################################################################
"########################  Gitgutter configuration  #######################
"##########################################################################


highlight SignColumn ctermbg=black

let g:gitgutter_override_sign_column_highlight = 0
let g:gitgutter_highlight_lines = 0
let g:gitgutter_map_keys = 0
let g:gitgutter_enabled = 1
let g:gitgutter_signs = 0

let g:gitgutter_max_signs = 5000

" Recommended in the GitGutter README.
set updatetime=100

nnoremap <leader>d :GitGutterSignsToggle<CR>

nmap ]g <Plug>GitGutterNextHunk
nmap [g <Plug>GitGutterPrevHunk

"##########################################################################
"########################     ALE configuration     #######################
"##########################################################################

" let g:ale_lint_on_enter = 0

" let g:ale_linters = { 'haskell': ['ghc-mod', 'hlint'], 'javascript': ['eslint']  }
"
" let g:ale_linters = { 'haskell': ['hlint'], 'javascript': ['eslint']  }

" autocmd FileType haskell nnoremap <buffer> <leader>? :call ale#cursor#ShowCursorDetail()<cr>



"##########################################################################
"#######################   Lightline configuration   ######################
"##########################################################################

set laststatus=2
set noshowmode

function! LightlineReadonly()
  return &readonly && &filetype !=# 'help' ? 'r' : ''
endfunction

let g:lightline = {
      \   'colorscheme': 'seoul256',
      \   'active': {
      \     'left': [ [ 'mode', 'paste' ],
      \               [ 'gitbranch', 'readonly', 'modified', 'buffers' ] ]
      \   },
      \   'component_expand': {
      \     'buffers': 'lightline#bufferline#buffers'
      \   },
      \   'component_type': {
      \     'buffers': 'tabsel'
      \   },
      \   'component_function': {
      \     'readonly':  'LightlineReadonly',
      \     'gitbranch': 'fugitive#head'
      \   }
      \ }


      " \     'filename':  'LightlineFilename'

let g:lightline#bufferline#show_number          = 2
let g:lightline#bufferline#unicode_symbols      = 1
let g:lightline#bufferline#number_separator     = ""
let g:lightline#bufferline#unnamed              = '[*]'
let g:lightline#bufferline#composed_number_map  = {
  \ 0: '⁰', 1: '¹', 2: '²', 3: '³', 4: '⁴',
  \ 5: '⁵', 6: '⁶', 7: '⁷', 8: '⁸', 9: '⁹'}


" I'm not sure if I'll keep these but it's worth giving it a go.
nmap <Leader>1 <Plug>lightline#bufferline#go(1)
nmap <Leader>2 <Plug>lightline#bufferline#go(2)
nmap <Leader>3 <Plug>lightline#bufferline#go(3)
nmap <Leader>4 <Plug>lightline#bufferline#go(4)
nmap <Leader>5 <Plug>lightline#bufferline#go(5)
nmap <Leader>6 <Plug>lightline#bufferline#go(6)
nmap <Leader>7 <Plug>lightline#bufferline#go(7)
nmap <Leader>8 <Plug>lightline#bufferline#go(8)
nmap <Leader>9 <Plug>lightline#bufferline#go(9)
nmap <Leader>0 <Plug>lightline#bufferline#go(10)

nmap <Leader>c1 <Plug>lightline#bufferline#delete(1)
nmap <Leader>c2 <Plug>lightline#bufferline#delete(2)
nmap <Leader>c3 <Plug>lightline#bufferline#delete(3)
nmap <Leader>c4 <Plug>lightline#bufferline#delete(4)
nmap <Leader>c5 <Plug>lightline#bufferline#delete(5)
nmap <Leader>c6 <Plug>lightline#bufferline#delete(6)
nmap <Leader>c7 <Plug>lightline#bufferline#delete(7)
nmap <Leader>c8 <Plug>lightline#bufferline#delete(8)
nmap <Leader>c9 <Plug>lightline#bufferline#delete(9)
nmap <Leader>c0 <Plug>lightline#bufferline#delete(10)

" function! LightlineFilename()
"   return expand('%') !=# '' ? expand('%') : '[No Name]'
" endfunction

autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()

"##########################################################################
"########################    gvim configuration     #######################
"##########################################################################

if has("gui_running")
  set guifont="Monospace 10"
  set guifont=Iosevka\ Term\ Medium

  set guioptions-=m  "remove menu bar
  set guioptions-=T  "remove toolbar
  set guioptions-=r  "remove right-hand scroll bar
  set guioptions-=l
  set guioptions-=R
  set guioptions-=b
endif



"########################################################################
"########################  Supertab configuration  ######################
"########################################################################

let g:SuperTabDefaultCompletionType = '<c-x><c-o>'

inoremap <Nul> <c-r>=SuperTabAlternateCompletion("\<lt>c-x>\<lt>c-o>")<cr>

let g:haskellmode_completion_ghc = 1



"########################################################################
"#######################   general haskell stuff  #######################
"########################################################################


autocmd FileType ruby nmap <C-]> :LspDefinition<CR>
autocmd FileType ruby nmap <Leader>h :LspHover<CR>
autocmd FileType ruby nmap <Leader>e :LspDocumentDiagnostics<CR>
autocmd FileType ruby nmap <Leader>l :LspCodeLens<CR>
autocmd FileType ruby nmap <Leader>a :LspCodeAction<CR>


"########################################################################
"#######################   general haskell stuff  #######################
"########################################################################

let g:lsp_settings_root_markers = ['.hie', '.git', '.git/']

let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('/tmp/vim-lsp.log')


au User lsp_setup call lsp#register_server({
    \ 'name': 'haskell-language-server',
    \ 'cmd': {server_info -> ['haskell-language-server-wrapper', '--lsp']},
    \ 'whitelist': ['haskell'],
    \ })

" 'efm-langserver': {'disabled': v:false}

autocmd FileType haskell nmap <C-]> :LspDefinition<CR>
autocmd FileType haskell nmap <Leader>h :LspHover<CR>
autocmd FileType haskell nmap <Leader>e :LspDocumentDiagnostics<CR>
autocmd FileType haskell nmap <Leader>l :LspCodeLens<CR>
autocmd FileType haskell nmap <Leader>a :LspCodeAction<CR>


function! JumpHaskellFunction(reverse)
    call search('\C[[:alnum:]]*\s*::', a:reverse ? 'bW' : 'W')
endfunction


" Set up [[ and ]] so they jump to the next and pervious functions respectively.
autocmd FileType haskell nnoremap <buffer><silent> ]] :call JumpHaskellFunction(0)<CR>
autocmd FileType haskell nnoremap <buffer><silent> [[ :call JumpHaskellFunction(1)<CR>

autocmd FileType haskell set iskeyword=@,a-z,A-Z,_,39,48-57


" Jump to the first import statement
autocmd FileType haskell nnoremap <buffer> gI gg /\cimport<CR><ESC>:noh<CR>

au FileType haskell nnoremap E :HoogleInfo<CR>


"########################################################################
"###############################   ctags  ###############################
"########################################################################

" Finally, once and for all, sort out the auto-genreation of the ctags
" file. That took a fucking lot of effort.

" This was taken from and is required for hothasktags:
"   https://www.reddit.com/r/vim/comments/520jil/tag_jumping_with_added_iskeyword/

" function! TagJumpHaskell()
"     let l:orig_keyword = &iskeyword
"     set iskeyword=a-z,A-Z,0-9,_,.,39,\.
"     let l:word = expand("<cWORD>")
"     echo l:word
"     let &iskeyword = l:orig_keyword
"     execute "tjump " . l:word
" endfunction

" autocmd FileType haskell nnoremap <C-]> :<C-u>call TagJumpHaskell()<CR>


" autocmd BufWritePost *.hs call system("zsh -c \"gr=$(git root); [[ $gr != $HOME ]] && { fast-tags --qualified (*~dist-newstyle)/**/*.hs; }\"")

" autocmd BufWritePost *.hs,*.cabal call system("git exec fast-tags --qualified (*~dist-newstyle)/**/*.hs *.hs(N.)")

" autocmd BufWritePost *.hs,*.cabal call system("gr=$(git root); [[ $gr != $HOME ]] && { cd $gr && fast-tags --qualified \$(find . -name \\*.hs -print -o \\( -name dist-newstyle -prune \\)) }")

autocmd BufWritePost *.c call system("gr=$(git root); [[ $gr != $HOME ]] && { cd $gr && ctags **/*.h **/*.c **/*.cpp && ctags --append --language-force=c++ **/*.ino; }")

autocmd BufWritePost *.py call system("gr=$(git root); [[ $gr != $HOME ]] && { cd $gr && ctags (*~ve)/**/*.py; }")

autocmd BufWritePost *.php call system("gr=$(git root); [[ $gr != $HOME ]] && { cd $gr && ctags -R --languages=php --fields=+aimS --php-kinds=cdfijv --tag-relative=yes --totals=yes --exclude=tags --extra=+q --exclude=vendor; }")


set tags+=tags;$HOME
set tags+=tags.haskdog;
set tags+=codex.tags;

" For vim/ctags to recognise methods with a ?
set iskeyword+=?


"########################################################################
"###########################   Experimental   ###########################
"########################################################################
"
" set shiftround

" This isn't quite right; it only puts you on the same line as the match.
" FIXME: I've tagged an Nn/Nn on the end of these mappings because the
" main command (as mentioned on the previous line) on moves teh cursor to the
" same line as the match. The n
" I'm sure there's a better way of doing this.
noremap ) :execute '/' . @*<CR> nN
noremap ( :execute '?' . @*<CR> Nn


function! MyFoldText()
    let line = getline(v:foldstart)
    let folded_line_num = v:foldend - v:foldstart
    let line_text = substitute(line, '^"{\+', '', 'g')
    let fillcharcount = &textwidth - len(line_text) - len(folded_line_num)
    return '+'. repeat(' ', 20) . line_text . repeat('.', fillcharcount) . '   (' . folded_line_num . ' L)'
endfunction

set foldtext=MyFoldText()

set fillchars=fold:\ 

" function! MyFoldText()
"   let nl = v:foldend - v:foldstart + 1
"   let comment = substitute(getline(v:foldstart),"^ *","",1)
"   let linetext = substitute(getline(v:foldstart+1),"^ *","",1)
"   let txt = '+ ' . linetext . ' : "' . comment . '" : length ' . nl
"   return txt
" endfunction
" set foldtext=MyFoldText()


"########################################################################
"###########################      php (!)     ###########################
"########################################################################

autocmd FileType php set redrawtime=10000


"########################################################################
"###########################      php (!)     ###########################
"########################################################################

set completeopt+=menuone
set completeopt+=noselect

let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#completion_delay = 1
