if [ "$PS1" ] ; then
  dotfiles=$HOME/.dotfiles

  # Setting $d to be the same as $dotfiles means cd ~d will work. Yah!
  d=$dotfiles

  private_dotfiles=$HOME/.dotfiles_private

  source $dotfiles/zsh/functions

  source_if_exists $dotfiles/zsh/config
  source_if_exists $dotfiles/zsh/vi-bindings

  source_if_exists $dotfiles/zsh/navigation

  # source_if_exists $dotfiles/zsh/bracketed-paste

  source_if_exists $dotfiles/zsh/key-bindings
  source_if_exists $dotfiles/zsh/prompt
  source_if_exists $dotfiles/zsh/aliases

  source_if_exists $dotfiles/zsh/direnv

  # Languages
  source_if_exists $dotfiles/zsh/haskell
  source_if_exists $dotfiles/zsh/rust

  source_if_exists $dotfiles/zsh/completion
  source_if_exists $dotfiles/zsh/config.$(short_hostname)

  # This must go after $dotfiles/zsh/completion
  source_if_exists $dotfiles/zsh/aws

  source_if_exists $private_dotfiles/ec2

  [ "$KBD_LAYOUT" = 'colemak' ] && source_if_exists $dotfiles/zsh/colemak
fi
